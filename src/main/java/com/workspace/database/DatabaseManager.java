package com.workspace.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.stereotype.Component;

@Component
public class DatabaseManager {

	// database parameters
	// TODO: add this to a configuration file
	private final String url = null;
	private final String user = null;
	private final String password = null;
	
    public void store() {
    	
    	try {
    	    Class.forName("com.mysql.jdbc.Driver");
    	}
    	catch (ClassNotFoundException e) {
        	System.out.println("Could not get connection driver");
    	}
    	
        String query = "SELECT VERSION()";
        
        try (Connection con = DriverManager.getConnection(url, user, password);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query)) {

            if (rs.next()) {
                
                System.out.println(rs.getString(1));
            }

        } catch (SQLException ex) {
        } 
    }
}

