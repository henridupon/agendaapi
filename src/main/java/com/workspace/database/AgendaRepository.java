package com.workspace.database;

import org.springframework.data.repository.CrudRepository;

import com.workspace.beans.Appointment;

//This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
//CRUD refers Create, Read, Update, Delete

public interface AgendaRepository extends CrudRepository<Appointment, Integer> {

}