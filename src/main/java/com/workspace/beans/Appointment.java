package com.workspace.beans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Appointment {
	
	private static DateTimeFormatter strictTimeFormat = DateTimeFormatter.ofPattern("HH:mm").withResolverStyle(ResolverStyle.STRICT);
	private static SimpleDateFormat strictDateFormat = new SimpleDateFormat("yyyy/MM/dd");

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	private Date date;
    private LocalTime startTime;
    private LocalTime endTime;
    
    public Appointment() {}

    public Appointment(Date date, LocalTime startTime, LocalTime endTime) {
    	this.date = dateParser(date);
        this.startTime = timeParser(startTime);
        this.endTime = timeParser(endTime);
    }
    
    public Appointment(Date date, LocalTime startTime) {
    	this.date = dateParser(date);
        this.startTime = timeParser(startTime);
    }
    
    public Integer getId() {
        return id;
      }

      public void setId(Integer id) {
        this.id = id;
      }

	public Date getDate() {
    	return date;
    }
	
	public void setDate(Date date) {
		this.date = date;
	}

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
    	this.startTime = timeParser(startTime);
    }

    public LocalTime getEndTime() {
        return endTime;
    }
    
    public void setEndTime(LocalTime endTime) {
    	this.endTime = timeParser(endTime);
    }

	@Override
	public String toString() {
		return "Appointment [date=" + date + ", start=" + startTime + ", end=" + endTime + "]";
	}
	
	private LocalTime timeParser(LocalTime time) {
		return LocalTime.parse(time.format(strictTimeFormat));
	}
	
	private Date dateParser(Date date) {
		try {
			return strictDateFormat.parse(strictDateFormat.format(date));
		} catch (ParseException e) {
			System.out.println(String.format("Could not format date=%s", date));
		}
		return null;
	}
      
}
