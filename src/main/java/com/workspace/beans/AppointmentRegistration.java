package com.workspace.beans;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.workspace.database.AgendaRepository;

@Component
public class AppointmentRegistration {
	
	@Autowired
	private AgendaRepository agendaRepository;
    
    public void storeAppointment(Appointment appointment) {
    	agendaRepository.save(appointment);
    }
    
    public boolean appointmentWouldOverlap(Appointment appointment) {
    	boolean overlap = false;
    	
    	if(getAppointments().isEmpty()) {
    		return false;
    	}
    	
    	for(Appointment appointmentElement : getAppointments()) {
    		if(appointment.getDate().equals(appointmentElement.getDate())) {
    			
        		if(bothEndAndStartAreAfter(appointment, appointmentElement) || 
        				bothEndAndStartAreBefore(appointment, appointmentElement)) {
        			overlap = false;
        		} else {
        			overlap = true;	
        		}        		    	
        	}
    	}
    	return overlap;
    }
    
    private boolean bothEndAndStartAreAfter(Appointment appointment, Appointment appointmentElement) {
    	return appointment.getEndTime().isAfter(appointmentElement.getEndTime()) &&
    			appointment.getStartTime().isAfter(appointmentElement.getStartTime());
    }
    
    private boolean bothEndAndStartAreBefore(Appointment appointment, Appointment appointmentElement) {
    	return appointment.getEndTime().isBefore(appointmentElement.getEndTime()) &&
    			appointment.getStartTime().isBefore(appointmentElement.getStartTime());	
    }    
    
    public Appointment getFirstAvailableTimeSlot(Appointment appointment) {
    	LocalTime compareStartTime = appointment.getStartTime();  
    	Date compareStartDate = appointment.getDate();  
    	
    	List<Appointment> filteredList = getFilteredList(appointment);
    	List<Appointment> sortedList = getSortedList(filteredList); 	

		for(Appointment app : sortedList) {	
	    	if(compareStartTime.isBefore(app.getStartTime())) {
	    		return new Appointment(compareStartDate, compareStartTime);
	    	}
	    	
			if(comparedTimeIsBetweenStartAndEnd(app.getStartTime(), app.getEndTime(), compareStartTime)) {
				// if the suggested time is within the appointment we need to set the time to the end time of that appointment
				compareStartTime = app.getEndTime();
			}			
		}
		return new Appointment(compareStartDate, compareStartTime);
    }
    
    private List<Appointment> getFilteredList(Appointment appointment) {
    	List<Appointment> listToSort = new ArrayList<Appointment>();    	
    	    	
    	for(Appointment appointmentElement : getAppointments()) {
    		
    		if(compareDates(appointment.getDate(), appointmentElement.getDate())) {    	
    			listToSort.add(appointmentElement); 
        	} 				    	
    	}
    	return listToSort;
    }
    
    private boolean compareDates(Date date1, Date date2) {
    	return  DateUtils.isSameDay(date1, date2); 
    }
    
    private List<Appointment> getSortedList(List<Appointment> listToSort) {
    	listToSort.sort(new Comparator<Appointment>() {
		    @Override
		    public int compare(Appointment a1, Appointment a2) {
		        if (a1.getStartTime().isBefore(a2.getStartTime())) return -1;
		        if (a1.getStartTime().isAfter(a2.getStartTime())) return 1;
		        return 0;
		    }
		});
    	return listToSort;		
    }

	private boolean comparedTimeIsBetweenStartAndEnd(LocalTime start, LocalTime end, LocalTime compareStart) {
		return (compareStart.equals(start) || (compareStart.isAfter(start) && compareStart.isBefore(end)));
	}

	public List<Appointment> getAppointments() {
		return StreamSupport 
                .stream(agendaRepository.findAll().spliterator(), false) 
                .collect(Collectors.toList());
	}
}