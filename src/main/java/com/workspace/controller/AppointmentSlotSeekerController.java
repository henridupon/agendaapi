package com.workspace.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.workspace.beans.Appointment;
import com.workspace.beans.AppointmentRegistration;

@Controller
public class AppointmentSlotSeekerController {
	
	@Autowired
	private AppointmentRegistration appointmentRegistration;

	
	@RequestMapping(method = RequestMethod.POST, value = "/agendaApi/geefEersteVrijeSlotVanaf")
	@ResponseBody
	public String findFirstNextSlot(@RequestBody Appointment appointment) {

		Appointment firstAvailable = appointmentRegistration.getFirstAvailableTimeSlot(appointment);
		
		String responseString = String.format("Available, Date: %s, Time: %s", firstAvailable.getDate(), firstAvailable.getStartTime());
		
		return responseString;
	}

}
