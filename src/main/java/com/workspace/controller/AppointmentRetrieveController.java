package com.workspace.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.workspace.beans.Appointment;
import com.workspace.beans.AppointmentRegistration;

@Controller
public class AppointmentRetrieveController {
	
	
	@Autowired
	private AppointmentRegistration appointmentRegistration;
	
	@RequestMapping(method = RequestMethod.GET, value = "/agendaApi/planAfspraak")
	@ResponseBody
	public List<Appointment> getAllApppointments() {
		return appointmentRegistration.getAppointments();		
	}
}