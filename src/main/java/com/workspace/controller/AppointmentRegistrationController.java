package com.workspace.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.workspace.beans.Appointment;
import com.workspace.beans.AppointmentRegistration;

@Controller
public class AppointmentRegistrationController {
	
	@Autowired
	private AppointmentRegistration appointmentRegistration;
	
	@RequestMapping(method = RequestMethod.POST, value = "/agendaApi/appointment")
	@ResponseBody
	public ResponseEntity<String> registerAppointment(@RequestBody Appointment appointment) {
		System.out.println(String.format("Registering appointment: %s", appointment));
		
		// check if the database already contains an appointment on that time slot
		boolean overlap = appointmentRegistration.appointmentWouldOverlap(appointment);
		
		if(! overlap) {
			
			// store if the appointments do not overlap
			appointmentRegistration.storeAppointment(appointment);			

		    return ResponseEntity
		    		.status(HttpStatus.CREATED)
		            .body(String.format("Appointment created: %s", appointment.toString()));
		}
		return ResponseEntity
				.status(HttpStatus.NOT_ACCEPTABLE)
				.body(String.format("Appointment: %s already exists.", appointment.toString()));
	    }

}