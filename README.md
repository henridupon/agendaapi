# AgendaAPI

This is a very small project created for an assignment. This project lets you create appointments and stores these in a database (agenda). Other calls the user can make 
are getting an overlook of the created appointments and get the next available time given a date and time.
	
## Create database
1. Open MySQL (this must be installed first)
2. When this is properly installed you must add the following lines to the application.properties file (change username and password) in the src/main/resources file:
	* spring.datasource.username=[your username]
	* spring.datasource.password=[your password]
3. When the properties are configured and MySQL is installed execute the following commands in MySQL:
 	* create database agenda;
	* create user '[your username]r'@'%' identified by '[your password]'; 
	* grant all on agenda.* to '[your username]'@'%';
	
## How to test
The AgendaAPI.postman_collection.json file located in the project contains three endpoint tests that can be used to test the API's (for instance in Postman).
These are:
<br/> http://localhost:8083/agendaApi/planAfspraak
<br/> http://localhost:8083/agendaApi/appointment
<br/> http://localhost:8083/agendaApi/geefEersteVrijeSlotVanaf

## How to run
Run the program by executing the next commands:
1. Open your terminal and open the AgendaAPI project. e.g.: cd C:/User/eclipse-projects/AgendaAPI
2. Execute the next command in your terminal: 
	```mvnw clean package```
3. Execute the next command in your terminal: 
	```java -jar target\API-0.0.1-SNAPSHOT.jar```